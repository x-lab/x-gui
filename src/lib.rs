#![allow(unused_imports)]

#[cfg(target_os = "macos")]
use cacao::macos::*;                          // TODO: remove that ----------------------------- //

use std::os::raw::{c_char, c_int, c_void};
use std::ffi::{CStr, CString};

use cpp::{cpp, cpp_class};

use qmetaobject::prelude::*;
use qmetaobject::qtdeclarative::*;
use qmetaobject::QObjectPinned;

// ////////////////////////////////////////////////////////////////////////////////////////////////
// xApplicationEngineHolder
// ////////////////////////////////////////////////////////////////////////////////////////////////

cpp! {{
#include <xApplicationWindow.hpp>
}}

static HAS_ENGINE: std::sync::atomic::AtomicBool = std::sync::atomic::AtomicBool::new(false);

cpp! {{
    struct SingleApplicationGuard {
        SingleApplicationGuard() {
            rust!(Rust_xApplicationEngine_ctor[] {
                HAS_ENGINE.compare_exchange(false, true, std::sync::atomic::Ordering::SeqCst, std::sync::atomic::Ordering::SeqCst)
                    .expect("There can only be one xApplication in the process");
            });
        }
        ~SingleApplicationGuard() {
            rust!(Rust_xApplicationEngine_dtor[] {
                HAS_ENGINE.compare_exchange(true, false, std::sync::atomic::Ordering::SeqCst, std::sync::atomic::Ordering::SeqCst)
                    .unwrap();
            });
        }
    };

    struct xApplicationEngineHolder : SingleApplicationGuard {
        std::unique_ptr<QApplication> app;
        std::unique_ptr<xApplicationWindow> window;

        xApplicationEngineHolder(int &argc, char **argv)
        : app(new QApplication(argc, argv))
        , window(new xApplicationWindow())
        {

        }
    };
}}

// ////////////////////////////////////////////////////////////////////////////////////////////////
// xApplicationEngine
// ////////////////////////////////////////////////////////////////////////////////////////////////

cpp_class!(
    pub unsafe struct xApplicationEngine as "xApplicationEngineHolder"
);

impl xApplicationEngine {

    pub fn new() -> xApplicationEngine {
        let mut arguments: Vec<*mut c_char> = std::env::args()
            .map(|arg| CString::new(arg.into_bytes()).expect("argument contains invalid c-string!"))
            .map(|arg| arg.into_raw())
            .collect();

        let argc: i32 = arguments.len() as i32 - 1;
        let argv: *mut *mut c_char = arguments.as_mut_ptr();

        let result = cpp!(unsafe [
            argc as "int",
            argv as "char **"
        ] -> xApplicationEngine as "xApplicationEngineHolder" {

            static int _argc  = argc;
            static char **_argv = nullptr;

            if (_argv == nullptr) {
                _argv = new char *[argc + 1];
                _argv[argc] = nullptr;
                for (int i = 0; i < argc; ++i) {
                    _argv[i] = new char[strlen(argv[i]) + 1];
                    strcpy(_argv[i], argv[i]);
                }
            }

            return xApplicationEngineHolder(_argc, _argv);
        });

        for arg in arguments {
            let _ = unsafe { CString::from_raw(arg) };
        }

        result
    }

    pub fn new_profiled() -> xApplicationEngine {
        let mut arguments: Vec<*mut c_char> = std::env::args()
            .map(|arg| CString::new(arg.into_bytes()).expect("argument contains invalid c-string!"))
            .map(|arg| arg.into_raw())
            .collect();

        arguments.push(CString::new("-qmljsdebugger=file:/tmp/1").expect("").into_raw());
        arguments.push(CString::new("").expect("").into_raw());

        let argc: i32 = arguments.len() as i32 - 1;
        let argv: *mut *mut c_char = arguments.as_mut_ptr();

        let result = cpp!(unsafe [
            argc as "int",
            argv as "char **"
        ] -> xApplicationEngine as "xApplicationEngineHolder" {

            static int _argc  = argc;
            static char **_argv = nullptr;

            if (_argv == nullptr) {
                _argv = new char *[argc + 1];
                _argv[argc] = nullptr;
                for (int i = 0; i < argc; ++i) {
                    _argv[i] = new char[strlen(argv[i]) + 1];
                    strcpy(_argv[i], argv[i]);
                }
            }

            return xApplicationEngineHolder(_argc, _argv);
        });

        for arg in arguments {
            let _ = unsafe { CString::from_raw(arg) };
        }

        result
    }

    pub fn set_application_name(&self, name: QString) {
        cpp!(unsafe [self as "xApplicationEngineHolder *", name as "QString"] {
            self->app->setApplicationName(name);
        })
    }

    pub fn set_organisation_name(&self, name: QString) {
        cpp!(unsafe [self as "xApplicationEngineHolder *", name as "QString"] {
            self->app->setOrganizationName(name);
        })
    }

    pub fn set_organisation_domain(&self, name: QString) {
        cpp!(unsafe [self as "xApplicationEngineHolder *", name as "QString"] {
            self->app->setOrganizationDomain(name);
        })
    }

    pub fn title(&self) -> QString {
        cpp!(unsafe [self as "xApplicationEngineHolder *"] -> QString as "QString" {
            return self->app->applicationName();
        })
    }

    pub fn engine(&self) -> *mut c_void {
        cpp!(unsafe [self as "xApplicationEngineHolder *"] -> *mut c_void as "QObject *" {
            return (QObject *)self->window->engine();
        })
    }

    pub fn view(&self) -> *mut c_void {
        cpp!(unsafe [self as "xApplicationEngineHolder *"] -> *mut c_void as "QObject*" {
            return (QObject *)self->window->view();
        })
    }

    pub fn add_import_path(&mut self, path: QString) {
        cpp!(unsafe [self as "xApplicationEngineHolder *", path as "QString"] {
            self->window->addImportPath(path);
        })
    }

    pub fn set_property(&mut self, name: QString, value: QVariant) {
        cpp!(unsafe [self as "xApplicationEngineHolder *", name as "QString", value as "QVariant"] {
            self->window->setContextProperty(name, value);
        })
    }

    pub fn set_object_property<T: QObject + Sized> (
        &mut self,
        name: QString,
        obj: QObjectPinned<T>,
    ) {
        let obj_ptr = obj.get_or_create_cpp_object();
        cpp!(unsafe [self as "xApplicationEngineHolder *", name as "QString", obj_ptr as "QObject *"] {
            self->window->setContextProperty(name, obj_ptr);
        })
    }

    pub fn set_source(&mut self, path: QString) {
        cpp!(unsafe [self as "xApplicationEngineHolder *", path as "QString"] {
            self->window->setSource(path);
        })
    }

    pub fn resize(&mut self, width: i16, height: i16) {
        cpp!(unsafe [self as "xApplicationEngineHolder *", width as "quint16", height as "quint16"] {
            self->window->resize(width, height);
        })
    }

    pub fn exec(&self) {
        cpp!(unsafe [self as "xApplicationEngineHolder *"] {
            self->window->show();
            self->window->raise();
            self->app->exec();
        })
    }

    pub fn quit(&self) {
        cpp!(unsafe [self as "xApplicationEngineHolder *"] {
            self->app->quit();
        })
    }
}
