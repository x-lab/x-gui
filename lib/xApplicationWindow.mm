#include "xApplicationWindow.hpp"

#include <QtGlobal>
#include <QtQml>
#include <QtQuick>
#include <QtQuick3D/qquick3d.h>

#include <AppKit/AppKit.h>

class xApplicationWindowPrivate
{
public:
    QWindow *window = nullptr;

public:
    QQuickView *view = nullptr;
};

xApplicationWindow::xApplicationWindow(QWindow *parent) : QObject()
{
    QSurfaceFormat::setDefaultFormat(QQuick3D::idealSurfaceFormat());
    
    NSVisualEffectView *vibrant = [[NSVisualEffectView alloc] init];
    vibrant.blendingMode = NSVisualEffectBlendingModeBehindWindow;
    vibrant.appearance = [NSAppearance appearanceNamed: NSAppearanceNameVibrantDark];

    d = new xApplicationWindowPrivate;

    d->window = QWindow::fromWinId(reinterpret_cast<WId>(vibrant));

    d->view = new QQuickView(d->window);
    d->view->setResizeMode(QQuickView::SizeRootObjectToView);
    d->view->setColor(QColor(Qt::transparent));

    d->view->engine()->rootContext()->setContextProperty("QtVersion", QString(qVersion()));

    NSWindow *self = [(NSView *)(d->window->winId()) window];
    self.titlebarAppearsTransparent = YES;
    self.styleMask = NSWindowStyleMaskTitled | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable | NSWindowStyleMaskClosable | NSWindowStyleMaskFullSizeContentView;

    connect(d->window, &QWindow::widthChanged, [=] (int width)
    {
        d->view->setWidth(width);
    });

    connect(d->window, &QWindow::heightChanged, [=] (int height)
    {
        d->view->setHeight(height);
    });

    connect(d->view->engine(), &QQmlEngine::quit, &QGuiApplication::quit);

    this->readSettings();
}

xApplicationWindow::~xApplicationWindow(void)
{
    this->writeSettings();

    delete d->window;
    delete d;
}

void xApplicationWindow::writeSettings(void)
{
    QSettings settings(QSettings::UserScope, "x", "quick");
    settings.beginGroup("window");
    settings.setValue("size", d->window->size());
    settings.setValue("pos", d->window->position());
    settings.endGroup();
}

void xApplicationWindow::readSettings(void)
{
    QSettings settings(QSettings::UserScope, "x", "quick");
    settings.beginGroup("window");
    d->window->resize(settings.value("size", QSize(1024, 800)).toSize());
    d->window->setPosition(settings.value("pos", QPoint(20, 20)).toPoint());
    settings.endGroup();
}

void xApplicationWindow::addImportPath(const QString& path)
{
    d->view->engine()->addImportPath(path);
}

void xApplicationWindow::setContextProperty(const QString& name, const QVariant& value)
{
    d->view->engine()->rootContext()->setContextProperty(name, value);
}

void xApplicationWindow::setContextProperty(const QString& name, QObject *value)
{
    d->view->engine()->rootContext()->setContextProperty(name, value);
}

void xApplicationWindow::setSource(const QUrl& source)
{
    d->view->setSource(source);
}

QQmlEngine *xApplicationWindow::engine(void)
{
    return d->view->engine();
}

QQuickView *xApplicationWindow::view(void)
{
    return d->view;
}

void xApplicationWindow::show(void)
{
    d->window->show();
    d->view->show();
}

void xApplicationWindow::raise(void)
{
    d->window->raise();
    d->window->requestActivate();
}

void xApplicationWindow::resize(quint16 width, quint16 height)
{
    d->window->resize(width, height);
}
