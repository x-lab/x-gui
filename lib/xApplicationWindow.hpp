#pragma once

#include <QtCore>
#include <QtGui>
#include <QtQml>
#include <QtQuick>
#include <QtWidgets>

class xApplicationWindow : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QQmlEngine *engine READ engine)

public:
     xApplicationWindow(QWindow *parent = nullptr);
    ~xApplicationWindow(void);

public:
    void  readSettings(void);
    void writeSettings(void);

public:
    void addImportPath(const QString&);

public slots:
    void setSource(const QUrl&);

public:
    QQmlEngine *engine(void);
    QQuickView *view(void);

public:
    void setContextProperty(const QString&, const QVariant&);
    void setContextProperty(const QString&, QObject *);

public slots:
    void show(void);
    void raise(void);

public:
    void resize(quint16, quint16);

private:
    class xApplicationWindowPrivate *d;
};
