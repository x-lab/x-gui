#include "xApplicationWindow.hpp"

#include <QtQml>
#include <QtQuick>
#include <QtQuickWidgets>
#include <QtQuick3D/qquick3d.h>

class xApplicationWindowPrivate
{
// public:
//     QQuickWidget *window = nullptr;

public:
    QQuickView *view = nullptr;
};

xApplicationWindow::xApplicationWindow(QWindow *parent) : QObject()
{
    QSurfaceFormat::setDefaultFormat(QQuick3D::idealSurfaceFormat());

    d = new xApplicationWindowPrivate;

    // d->window = new QQuickWidget;
    // d->window->setAttribute(Qt::WA_NoSystemBackground, true);
    // d->window->setAttribute(Qt::WA_TranslucentBackground, true);

    d->view = new QQuickView(parent);
    d->view->setResizeMode(QQuickView::SizeRootObjectToView);

    d->view->setColor(QColor(Qt::transparent));

    // connect(d->window->window(), &QWindow::widthChanged, [=] (int width)
    // {
    //     d->view->setWidth(width);
    // });

    // connect(d->window->window(), &QWindow::heightChanged, [=] (int height)
    // {
    //     d->view->setHeight(height);
    // });

    connect(d->view->engine(), &QQmlEngine::quit, &QGuiApplication::quit);

    this->readSettings();
}

xApplicationWindow::~xApplicationWindow(void)
{
    this->writeSettings();

    delete d->view;
    delete d;
}

void xApplicationWindow::writeSettings(void)
{
    QSettings settings(QSettings::UserScope, "x", "quick");
    settings.beginGroup("window");
    settings.setValue("size", d->view->size());
    settings.setValue("pos", d->view->position());
    settings.endGroup();
}

void xApplicationWindow::readSettings(void)
{
    QSettings settings(QSettings::UserScope, "x", "quick");
    settings.beginGroup("window");
    d->view->resize(settings.value("size", QSize(1024, 800)).toSize());
    d->view->setPosition(settings.value("pos", QPoint(20, 20)).toPoint());
    settings.endGroup();
}

void xApplicationWindow::addImportPath(const QString& path)
{
    d->view->engine()->addImportPath(path);
}

void xApplicationWindow::setContextProperty(const QString& name, const QVariant& value)
{
    d->view->engine()->rootContext()->setContextProperty(name, value);
}

void xApplicationWindow::setContextProperty(const QString& name, QObject *value)
{
    d->view->engine()->rootContext()->setContextProperty(name, value);
}

void xApplicationWindow::setSource(const QUrl& source)
{
    d->view->setSource(source);
}

QQmlEngine *xApplicationWindow::engine(void)
{
    return d->view->engine();
}

QQuickView *xApplicationWindow::view(void)
{
    return d->view;
}

void xApplicationWindow::show(void)
{
    d->view->show();
}

void xApplicationWindow::raise(void)
{
    d->view->raise();
    d->view->requestActivate();
}

void xApplicationWindow::resize(quint16 width, quint16 height)
{
    d->view->resize(width, height);
}
